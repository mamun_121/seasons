import React from 'react';
import './App.css';

class App extends React.Component {
  render() {
    navigator.geolocation.getCurrentPosition(
        position => console.log(position.coords.latitude, position.coords.longitude),
        err => console.log(err)
    )
    return(
      <div>
        Hi, there!
      </div>
    );
  }
}

export default App;
